from setuptools import setup

setup(name='Segmentor',
      version='0.1',
      description='Image Segmentation',
      url='https://gitlab.com/Jimbo51000/Felix-3D.git',
      author='Jimmy Joseph',
      author_email='jimmyj005@gmail.com',
      license='GPL',
      packages=setuptools.find_packages()
      install_requires=[
          'numpy','scikit-learn',
          'rawpy','imageio',
          'scikit-image','opencv-python'
      ],
      zip_safe=False,
      dependency_links=[
          'https://pypi.org/project/'
      ]
      )
