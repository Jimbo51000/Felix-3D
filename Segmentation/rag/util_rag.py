import numpy as np
import argparse
import cv2
import os,shutil
import time
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import Counter
from itertools import chain
import rawpy
import imageio
from matplotlib import pyplot as plt
import util_segment as us
from skimage.measure import compare_ssim

from skimage.segmentation import slic
from skimage.segmentation import felzenszwalb
from skimage.data import coffee
from skimage.color import label2rgb
from skimage.segmentation import mark_boundaries
from skimage.future import graph
from skimage import data, segmentation, color, filters, io
from skimage.measure import regionprops
from skimage import draw
from skimage.util import img_as_ubyte

from scipy.spatial import distance
import scipy.spatial as spatial
import scipy.spatial.distance as dist
import scipy.cluster.hierarchy as hier

from functools import reduce

def resize(im,ratio=0.3):
    img =  im.copy()
    dim = (int(img.shape[1]*ratio),int(img.shape[0]*ratio))
    image = cv2.resize(img,dim ,interpolation = cv2.INTER_AREA)
    return image

def get_slic_segments(img, n=500, c=10):
	segments_slic = slic(img, n_segments=n, compactness=c,max_iter=20)
	segments_slic = segments_slic + 1
	return segments_slic

def get_edge_length(rag,func=min):
    edges = get_edges(rag)
    l = list(map(lambda d: list(d.values()),list(edges.values())))
    r = reduce(lambda a,b : a+b ,l)
    return func(r)

def get_edges(rag):
    
    edges = {}
    
    for ni in rag.nodes:
        node = rag.node[ni]
        x1,y1 = node['centroid']
        src = node['labels'][0]
        n_dict = {}
        for n in rag.neighbors(src):
            neighbour = rag.node[n]
            x2,y2 = neighbour['centroid']
            dest = neighbour['labels'][0]
            l = distance.euclidean([x1,y1], [x2,y2])
            n_dict[dest] = l
        edges[src] = n_dict
        n_dict= None
    return edges


def display_edges(image, rag, threshold,req_labels=None):

    im = image.copy()
    for edge in rag.edges():
        n1, n2 = edge
 
        r1, c1 = map(int, rag.node[n1]['centroid'])
        r2, c2 = map(int, rag.node[n2]['centroid'])
        
        
        line  = draw.line(r1, c1, r2, c2)
        circle = draw.circle(r1,c1,5)
        
        if rag[n1][n2]['weight'] < threshold :
            im[line] = 0,0,0

        im[circle] = 0,1,0
        
    return im


def getReshapedContour(thresh):
    ct = us.getTheLargestContour(thresh,cv2.CHAIN_APPROX_NONE)
    cont_c = ct.copy()
    cont_c = list(map(lambda row: row.reshape((2,)), cont_c))
    cont_c = np.array(cont_c)
    return cont_c

def intersection(points1, points2, eps):
    tree = spatial.KDTree(points1)
    distances, indices = tree.query(points2, k=1, distance_upper_bound=eps)
    intersection_points = tree.data[indices[np.isfinite(distances)]]
    return intersection_points , indices , distances

def get_roi_equi_points(img,contour,rag):
	roi_equi_points = []

	cont_c = contour.copy()
	cont_c = list(map(lambda row: row.reshape((2,)), cont_c))
	cont_c = np.array(cont_c)

	min_edge_dist = get_edge_length(rag,min)

	current_point = cont_c[0]

	i = 0
	roi_equi_points = []
	roi_equi_points.append(current_point)
	while True:
	    
	    (cx,cy) = (current_point[0], current_point[1])
	    cir_img = np.zeros(img.shape[:2],np.uint8)
	    cv2.circle(cir_img,(cx,cy),int(min_edge_dist),255)
	    points = getReshapedContour(cir_img)

	    intersection_points , indices , distances = intersection(cont_c , points,2)
	    idx = min(indices)
	    
	    if idx > len(cont_c) - 1 :
	        break
	    current_point = cont_c[idx]
	    cont_c = cont_c[idx:,:]
	    roi_equi_points.append((cy,cx))
	    
	    i = i + 1

	return np.array(roi_equi_points)


def get_all_region_centroids(regions):
    centroids = {}
    for region in regions:
        (x,y) = region['centroid']
        #print(region.label)
        #print(x,y)
        centroids[region.label] = (int(x),int(y))
    return centroids


def find_nearest_neighbour(points1 , point , eph):
    tree = spatial.KDTree(points1)
    distances, indices = tree.query(point, k=1, distance_upper_bound=eph)
    #intersection_points = tree.data[indices[np.isfinite(distances)]]
    return indices[0]

def find_nn_roi(img,contour,rag,regions,centroids):
	max_edge_dist = get_edge_length(rag,max)
	nn_max_dist = 2 * max_edge_dist

	centroids_list = list(map(list,list(centroids.values())))
	centroids_labels = list(centroids.keys())

	roi_equi_points = get_roi_equi_points(img,contour,rag)

	req_centroids = {}
	for i , point in enumerate(roi_equi_points):
	    #print(i)
	    idx = find_nearest_neighbour(centroids_list,[point],nn_max_dist)
	    if idx > len(centroids_list) - 1:        
	        break
	    label = centroids_labels[idx]
	    region_index = label-1
	    while True:
	        cx , cy = regions[region_index].centroid[0] , regions[region_index].centroid[1]
	        if cv2.pointPolygonTest(contour,(cy,cx),False) <= 0 : #outside or on contour
	            break
	        else:
	            del centroids_list[idx]
	            del centroids_labels[idx]
	            idx = find_nearest_neighbour(centroids_list,[point],nn_max_dist)
	            if idx > len(centroids_list) - 1:
	                break
	            label = centroids_labels[idx]
	            region_index = label-1

	    req_centroids[label] = regions[region_index].centroid
	    
	return req_centroids


def get_nodes(centroids,contour,loc='inside'):
	nodes = np.array([])
	for label in list(centroids.keys()):
		centroid = centroids.get(label)
		cx , cy = centroid[0] , centroid[1]
		if loc =='inside':
			if cv2.pointPolygonTest(contour,(cy,cx),False)>0:
				nodes = np.append(nodes,label)
		else:
			if cv2.pointPolygonTest(contour,(cy,cx),False)<=0:
				nodes = np.append(nodes,label)
	return nodes

def common_members(a, b):

    a_set = set(a) 
    b_set = set(b) 
    return a_set & b_set


def get_bg_threshold(contour,rag,centroids):
	threshold = 0.0
	t = np.array([])
	outside_nodes = get_nodes(centroids,contour,'outside')
	for edge in rag.edges():
	    n1, n2 = edge
	    if np.any(outside_nodes == n1) and np.any(outside_nodes == n2):
	        t = np.append(t,rag[n1][n2]['weight'])
	threshold = np.mean(t)

	return threshold


def get_segmented_nodes(img,roi_contour,rag,regions,centroids):

	bg_centroids = find_nn_roi(img,roi_contour,rag,regions,centroids)
	threshold = get_bg_threshold(roi_contour,rag,centroids)
	bg_centroid_labels = list(bg_centroids.keys())
	idx = 0
	inside_nodes = get_nodes(centroids,roi_contour,'inside')

	while True:
	    if inside_nodes.size==0:
	        break
	    new_labels = np.array([])
	    for src in inside_nodes:
	        c_m = list(common_members(bg_centroid_labels ,list(rag.neighbors(src))))
	        #find the edges from src to each common member
	        edge_w = np.array([rag[src][m]['weight'] for m in c_m])
	        if np.any(edge_w <= threshold):
	            bg_centroid_labels.append(src)
	            new_labels = np.append(new_labels , src) 
	                             
	    if new_labels.size==0:
	        break
	    else:
	        inside_nodes = inside_nodes[~np.in1d(inside_nodes, new_labels)]


	return inside_nodes

def get_mask_from_nodes(segments_slic,inside_nodes):
	mask = segments_slic.copy()
	dim = mask.shape
	mask[~np.in1d(np.ravel(mask), inside_nodes).reshape(dim)] = 0
	mask[mask>0] =  1
	mask = mask.astype('uint8')

	return mask