import numpy as np
import argparse
import cv2
import os,shutil
import time
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import Counter
from itertools import chain
import rawpy
import imageio
from matplotlib import pyplot as plt

from skimage.measure import compare_ssim

from skimage.segmentation import slic
from skimage.segmentation import felzenszwalb
from skimage.data import coffee
from skimage.color import label2rgb
from skimage.segmentation import mark_boundaries
from skimage.future import graph
from skimage import data, segmentation, color, filters, io
from skimage.measure import regionprops
from skimage import draw
from skimage.util import img_as_ubyte

from scipy.spatial import distance
import scipy.spatial as spatial
import scipy.spatial.distance as dist
import scipy.cluster.hierarchy as hier

from functools import reduce

import multiprocessing
from multiprocessing import Pool
import logging
import traceback

import argparse
from functools import reduce
from functools import partial
import sys

import util_segment as us
import util_rag as ur
import util_grabcut as ug

def createAppLogger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler('./logs/app.log' , mode = file_mode)
    fmt = '%(asctime)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def worker_roi(file_group,path,logger):
    try:
        return ug.find_ssim_mask(path,file_group)
        sys.stdout.flush()
    except Exception as e:
        logger.error('Thread failure for worker ROI sub-group %s ',file_group , exc_info=True)




def run_roi_calculation():
    print('Calculating ROIs')
    logger.info('Calculating ROIs')
    for key in groups:
        try:
            #build subtasks
            g , group = key , groups[key]
            roi_mask = None
            if roi_folder:
                roi_mask = cv2.imread(os.path.join(roi_folder,roi_files[g]),0)
            else:
                group_size = len(group)
                sub_size = (int) (group_size/num_processors)
                sub_groups = np.split(group , [i*sub_size for i in range(1,num_processors)])
                worker_roi_func = partial(worker_roi,path=path,logger=logger)
                with multiprocessing.Pool(processes = num_processors) as p:
                #get them to work in parallel
                    output_ssims = p.map(worker_roi_func,sub_groups)
                    roi_mask = reduce(ug.sumUpDiff, output_ssims)
                    (he, wi) = roi_mask.shape[:2]
                    roi_mask = roi_mask[100:he - 100, 100:wi - 100]
                    p.close()
                    p.join()


            roi_mask = us.getObjectOnlyMask(roi_mask)
            roi_mask = roi_mask.astype('uint8')
            roi_masks[g] = roi_mask
            #roi_bar.update(roi_curr_i+1)
            #roi_curr_i+=1
            logger.info('ROI Mask Calculation done for group %s' , group)
        except Exception as e:
            logger.error('Thread failure for run ROI group %s ',group , exc_info=True)
    logger.info('All ROI Mask Calculation done')
    print('ROI Calculation done')


def worker_bg_reconstruction(task,path,roi_masks,scale_ratio,logger):
	parameters = [
		{
		    'n_segments':800,
		    'compactness':50
		}
		,


		{
		    'n_segments':800,
		    'compactness':60
		}

	]

	g , f = task

	try:

		original_image = None
		original_image = cv2.imread(os.path.join(path,f))

		if original_image is not None:
			roi_mask = roi_masks[g]
			roi_mask[roi_mask>0] = 1
			roi_mask_orig = roi_mask.astype('int')
			roi_mask = ur.resize(roi_mask,scale_ratio)
			roi_contour = us.getTheLargestContour(roi_mask,cv2.CHAIN_APPROX_NONE)

			centroids = None
			mask = None
			inside_nodes = None
			regions= None
			rag = None
			segments_slic= None
			final_img = None

			original_image = cv2.imread(os.path.join(path,f))
			(he,wi) = original_image.shape[:2]
			original_image = original_image[100:he-100,100:wi-100]


			#tune here
			original_image = cv2.bilateralFilter(original_image,9,75,75)

			img = ur.resize(original_image,scale_ratio)
			all_masks = []
			fg_final_mask = np.zeros(original_image.shape[:2])
			bg_final_mask = np.zeros(original_image.shape[:2])
			for i,parameter in enumerate(parameters):
			    n_segments = parameter['n_segments']
			    compactness = parameter['compactness']
			    #print('Parameter '+str(i+1)+':'+str(parameter))
			    #tune here
			    segments_slic = ur.get_slic_segments(img,n=n_segments,c=compactness)

			    rag = graph.rag_mean_color(img, segments_slic)

			    regions = regionprops(segments_slic)
			    for region in regions:
			        rag.node[region['label']]['centroid'] = region['centroid']

			    centroids = ur.get_all_region_centroids(regions)

			    inside_nodes = ur.get_segmented_nodes(img,roi_contour,rag,regions,centroids)
			    #all_nodes = np.array(list(centroids.keys()))
			    #all_other_nodes = all_nodes[~np.in1d(all_nodes, inside_nodes)]
			    mask = ur.get_mask_from_nodes(segments_slic,inside_nodes)
			    mask = cv2.resize(mask, (original_image.shape[1] , original_image.shape[0]))
			    #mask[mask>0] = 255
			    mask = mask.astype('int')
			    fg_final_mask += mask

			fg_final_mask[fg_final_mask>0] = 1

			fg_final_mask = fg_final_mask * roi_mask_orig

			bg_final_mask[fg_final_mask==0] = 1

			bg_final_mask = bg_final_mask.astype('uint8')
			final_img = original_image*bg_final_mask[:,:,np.newaxis]
			#us.writeImage(outfolder , f , final_img)
			logger.info('%s bg mask done ,roi_mask_id = %d , parameters :%s',f,(g+1),str(parameters))

			return final_img

	except Exception as e:
		logger.error('%s loading failed ',f , exc_info=True)

def worker_compute_bg(task,logger,outfolder):
	g , all_bg_images = task
	logger.info('Constructing background from bg masks for group %d ',(g+1))
	print('Constructing background from bg masks for group '+str(g+1))
	all_bg_images = np.array(all_bg_images)
	bg_image = np.zeros(all_bg_images[0].shape)

	#Gets the max values along each channel for each pixel location ,
	#the max function recevies a 1-D array of pixel values
	bg_image = np.apply_along_axis(max,0,all_bg_images)
	bg_image = bg_image.astype('uint8')

	#3.
	us.writeImage(outfolder , str(g+1)+'_bg_reconst.png' , bg_image)
	logger.info('Background Constructed for group %d ',(g+1))

def worker_compute_bg_split(task,logger,outfolder):
    '''
        Reconstructs the bg from the the current block
    '''
	all_bg_images = task
	#logger.info('Constructing background from bg masks for group %d ',(g+1))
	#print('Constructing background from bg masks for group '+str(g+1))
	all_bg_images = np.array(all_bg_images)
	bg_image = np.zeros(all_bg_images[0].shape)

	#Gets the max values along each channel for each pixel location ,
	#the max function recevies a 1-D array of pixel values
	bg_image = np.apply_along_axis(max,0,all_bg_images)
	bg_image = bg_image.astype('uint8')

	#3.
    return bg_image

def split_tasks_by_memory(all_groups_bg_images,size_threshold = 1073741824):
    group_tasks = np.array([(key,all_groups_bg_images[key]) for key in all_groups_bg_images.keys()])
    group_tasks_group_by_size = np.array([])
    sub_group_tasks = np.array([])
    i = 0
    if group_tasks.nbytes > size_threshold:
        for group in group_tasks:


            if group[1].nbytes < size_threshold and sub_group_tasks.nbytes < size_threshold:
                group_tasks_group_by_size = np.append(group_tasks_group_by_size,sub_group_tasks)
                sub_group_tasks = np.append(sub_group_tasks,(group[0],group[1]))
            else if sub_group_tasks.nbytes >= size_threshold:
                group_tasks_group_by_size = np.append(group_tasks_group_by_size,sub_group_tasks)
                sub_group_tasks = np.array([])
            #splitting a single group into sub groups
            else if group[1].nbytes >= size_threshold:
                if sub_group_tasks.size>0:
                    group_tasks_group_by_size = np.append(group_tasks_group_by_size,sub_group_tasks)
                    sub_group_tasks = np.array([])
                    n = group[1].shape[0]
                    no_sub_groups = math.ceil(group[1].nbytes / size_threshold)
                    sub_group_size = (int)(n/no_sub_groups)
                    sub_sub_group_tasks = np.split(group[1],[j*sub_group_size for j in range(1,no_sub_groups)])
                    for sub_group in sub_sub_group_tasks:
                        group_tasks_group_by_size = np.append(group_tasks_group_by_size,np.array([(group[0],sub_group)]))
    else:
        group_tasks_group_by_size = np.append(group_tasks_group_by_size,group_tasks)

    return group_tasks_group_by_size


def stitch_bg_blocks(g,group_images_crop_blocks,outfolder,logger):
    prev = 0
    val = np.sqrt(len(group_images_crop_blocks)).astype('int')

    rows = []
    for i in range(val,len(group_images_crop_blocks)+1,val):
        row = np.array([])
        print(prev,i)
        for b in group_images_crop_blocks[prev:i]:

            if row.size==0:
                print('here')
                row = b
            else:
                row = np.concatenate((row,b),axis = 1)
        rows.append(row)
        prev = i

    final_img = np.array([])
    for row in rows:
        if final_img.size == 0:
            final_img = row
        else:
            final_img = np.concatenate((final_img,row),axis=0)

    us.writeImage(outfolder , str(g+1)+'_bg_reconst.png' , final_img)
	logger.info('Background Constructed for group %d ',(g+1))

def run_bg_reconstruction(size_threshold = 1073741824):
	#print('Starting bg reconstruction')
	all_groups_bg_images = {}
	try:
		for key in groups:

			g , group = key , groups[key]
			tasks = [(g,f) for f in group]
			#1.

			worker_bg_reconstruction_func = partial(worker_bg_reconstruction,path=path,roi_masks=roi_masks,scale_ratio=scale_ratio,logger=logger)
			all_bg_images = []
			print('Computing RAG for group '+str(g+1))
			with multiprocessing.Pool(processes = num_processors) as p:
				all_bg_images = p.map(worker_bg_reconstruction_func,tasks)
				p.close()
				p.join()

			all_groups_bg_images[g] = all_bg_images
            ##############################################################################

            # CHANGED SECTION

            ###############################################################################
            all_bg_images = np.array(all_bg_images)

            #Memory check logic
            #all_bg_images shape : GxHxWx3
            if all_bg_images.nbytes > size_threshold:
                h,w = all_bg_images.shape[1:3]
                #grid size
                for i in range(2,10):
                    #print(i)
                    y_idx = [int(j*(h/i)) for j in range(1,i)]
                    x_idx = [int(j*(w/i)) for j in range(1,i)]
                    #first block
                    #print(y_idx,x_idx)
                    if all_bg_images[:,:y_idx[0],:x_idx[0]].nbytes > size_threshold:
                        continue
                    else:
                        #print('here')
                        group_images_crop_blocks = []
                        row_wise_split = np.split(all_bg_images,y_idx,axis=1)
                        #print(len(row_wise_split))

                        #print('Done')

                        for row_strips in row_wise_split:
                            #print('Strip {}'.format(row_strips.shape))
                            column_wise_split_for_strip = np.split(row_strips,x_idx,axis=2)
                            # for j in range(len(column_wise_split_for_strip)):
                            #     print(column_wise_split_for_strip[j].shape)
                            #print('Next row')
                            #print('Before {}'.format(len(group_images_crop_blocks)))
                            #print(column_wise_split_for_strip)
                            for block in column_wise_split_for_strip:
                                group_images_crop_blocks.append(block)

                        break
                # group_images_crop_blocks is a 1D list which contains all the crops and is of size i^2 .Each block is of size 1 GB
                # multiprocessing map return values in same order
                worker_compute_bg_func = partial(worker_compute_bg_split,logger=logger,outfolder=outfolder)
				#group_tasks = [(key,all_groups_bg_images[key]) for key in all_groups_bg_images.keys()]
                if len(group_images_crop_blocks) > 0:
                    all_bg_blocks_images = []
                    loop_times = np.ceil(len(group_images_crop_blocks)/num_processors).astype('int')
                    loop_times = max(1,loop_times)
                    remaining = len(group_images_crop_blocks)
                    for k in range(loop_times):
                        group_tasks = [group_images_crop_blocks[i + k * num_processors] for i in range(min(remaining ,num_processors))]
                        bg_blocks_images = []
        				with multiprocessing.Pool(processes = num_processors) as p2:
        				    bg_blocks_images = p2.map(worker_compute_bg_func,group_tasks)
        					p2.close()
        					p2.join()
                        for block_bg in bg_blocks_images:
                            all_bg_blocks_images.append(block_bg)
                        remaining -= num_processors


                #stitch back image
                #all_bg_blocks_images will be 1D list of length :   len(group_images_crop_blocks) . like 4,9
                stitch_bg_blocks(g,all_bg_blocks_images,outfolder,logger)

                #Removing to prevent the code below to run again
                if g in all_groups_bg_images:
                    del all_groups_bg_images[g]
                #all_groups_bg_images[g] = []
                #######################################################################################
	        #2.
			group_c = len(list(all_groups_bg_images.keys()))
			#print(group_c)
			if group_c == num_processors:
				worker_compute_bg_func = partial(worker_compute_bg,logger=logger,outfolder=outfolder)
				group_tasks = [(key,all_groups_bg_images[key]) for key in all_groups_bg_images.keys()]
				with multiprocessing.Pool(processes = num_processors) as p2:
					all_bg_images = p2.map(worker_compute_bg_func,group_tasks)
					p2.close()
					p2.join()

				all_groups_bg_images = {}

		if all_groups_bg_images!={}:
			worker_compute_bg_func = partial(worker_compute_bg,logger=logger,outfolder=outfolder)
			group_tasks = [(key,all_groups_bg_images[key]) for key in all_groups_bg_images.keys()]
			with multiprocessing.Pool(processes = num_processors) as p2:
				all_bg_images = p2.map(worker_compute_bg_func,group_tasks)
				p2.close()
				p2.join()

	except Exception as e:
		logger.error('Thread failure for run_bg_reconstruction %s ',group , exc_info=True)

#main function
if __name__ == '__main__':
    ap = argparse.ArgumentParser(prog = "segmentor",description='Performs background reconstruction from image sequences')
    ap.add_argument("-p", "--path", required=True,help="path to input image folder")
    ap.add_argument("-o", "--out", required=True,help="path to output image folder.New one will be created if not exists")
    ap.add_argument("-r", "--roi", required=False,help="path to roi image folder")
    ap.add_argument("-g", "--group_size", required=True,type = int ,help="Size of sequence of images of each camera position")
    ap.add_argument("-n", "--num_processors", required=False,type = int, help="No of processes to be executed over")
    ap.add_argument("-s", "--scale_ratio", required=False, help="Scale ratio to which the image must be resized for faster processing")
    ap.add_argument("-c", "--clearlogs", required=False, help="clear all the logs",action = 'store_true')

    args = vars(ap.parse_args())
    path = args["path"]
    outfolder = args["out"]
    roi_folder = args["roi"]
    group_size = args["group_size"]
    num_processors = args["num_processors"]
    scale_ratio = args["scale_ratio"]
    if not scale_ratio:
        scale_ratio = 0.3

    if args["clearlogs"]:
        file_mode = 'w'
    else:
        file_mode = 'a'
    #Global Variables

    roi_masks = {}
    groups = {}
    img_curr_i = 0
    roi_curr_i = 0

    PROCESSES = multiprocessing.cpu_count() - 1
    if num_processors == None:
    	num_processors = PROCESSES

    if os.path.isdir(outfolder):
        shutil.rmtree(outfolder, ignore_errors=False, onerror=None)
    os.mkdir(outfolder)

    logsfolder = './logs'
    if not os.path.isdir(logsfolder):
        os.mkdir(logsfolder)

    all = np.array([])

    files = sorted(os.listdir(path))
    if not roi_folder:
        roi_files = None
    else:
        roi_files = sorted(os.listdir(roi_folder))

    #All the images to be processed will be in groups varible and available globally
    files = np.array(files)
    m = (int)(len(files)/group_size)
    files_group = np.split(files , [i*group_size for i in range(1,m)])
    groups = {i:group for i,group in enumerate(files_group)}

    n_files = len(files)


    #logging.basicConfig(filename='./logs/app.log', filemode=file_mode, level = 'DEBUG' , format='%(asctime)s - %(levelname)s - %(message)s')
    logger = createAppLogger()
    print('Started Processing ')
    logger.info('Started Processing , \npath = %s , \n outpath = %s \n group_size = %d , num_processors = %d , scale_ratio = %.2f ',path,outfolder,group_size,num_processors,scale_ratio)
    logger.info('n_files = %d',len(files))
    start_time = time.time()


    run_roi_calculation()

    run_bg_reconstruction()

    logger.info("Completed in  %s seconds \n" , (time.time() - start_time))
    print("Completed in  %s seconds " % (time.time() - start_time))
