
import numpy as np
import argparse
import cv2
import os,shutil
import time
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import Counter
from itertools import chain
import rawpy
import imageio


# In[20]:


def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper)
 
	# return the edged image
	return edged


# In[21]:


def processImage(img_c, channel):
    a = img_c[:,:,channel]
    filtered = cv2.bilateralFilter(a, 7, 50, 150)
    filtered = cv2.Canny(filtered, 50, 200)
    return filtered 


# In[22]:


def drawContours(img_c, thresh):
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse = True)[:]
    for c in contours:
        #peri = cv2.arcLength(c, True)
        #approx = cv2.approxPolyDP(c, 0.01 * peri, True)
        approx = c
        cv2.drawContours(img_c, [approx], -1, (0, 255, 0), 5)
    return img_c


# In[23]:


def writeImage(outfolder , filename , img_write , extension=None):
    name, ext = filename.split('.')
    if extension is not None:
        ext = extension
    out_name = name+'_out'+'.'+ext
    cv2.imwrite(os.path.join(outfolder,out_name),img_write)
    #print('Writing to '+os.path.join(outfolder,out_name))


# In[83]:


def getTheLargestContour(thresh,approx=cv2.CHAIN_APPROX_SIMPLE):
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,approx)
    hierarchy = hierarchy[0]
    hierarchy = np.hstack((hierarchy,np.arange(len(hierarchy)).reshape((len(hierarchy),1))))
    
    new_contours_approx = []
    for c in contours:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.001 * peri, True)
        new_contours_approx.append(approx)
    
    (new_contours_approx , contours_sorted , hierarchy_sorted ) = zip(*sorted(zip(new_contours_approx,contours,hierarchy)
                                                                              , key=lambda c : cv2.contourArea(c[0]),
                                                                              reverse = True))
    
    parent = hierarchy_sorted[0]
    if len(hierarchy_sorted) > 1:
        child = hierarchy_sorted[1]
    else:
        return contours_sorted[0]
    
    if parent[2]==child[4] and child[3]==parent[4]:
        #choose the inner
        if child[0]==-1:
            #print('inner')
            return contours[child[4]]
    #print('outer')    
    return contours_sorted[0]


# In[25]:


def displayImage(img):
    out = cv2.resize(img, (600,600))
    cv2.imshow('output',out)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


# In[26]:


def getTheDominantColor(img,ratio = 0.5):
    image = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    size = (int(image.shape[1]*ratio),int(image.shape[0]*ratio))
    image = cv2.resize(image,size , 
                            interpolation = cv2.INTER_AREA)
    
    # reshape the image into a feature vector so that k-means
    # can be applied
    image = image.reshape((image.shape[0] * image.shape[1], 3))

    # apply k-means using the specified number of clusters and
    # then create the quantized image based on the predictions
    clt = MiniBatchKMeans(n_clusters = 2)
    labels = clt.fit_predict(image)

    #count labels to find most popular
    label_counts = Counter(labels)

    #subset out most popular centroid
    dominant_color = clt.cluster_centers_[label_counts.most_common(1)[0][0]]

    return list(dominant_color)


# In[27]:


def invertMask(mask,invert):
    res = mask.copy()
    if not invert:
        return res
    res = res-2
    res[res==-2]=1
    res[res==-1]=0
    return res


# In[28]:


def convertMasktoImage(mask):
    img = mask.copy()
    img[img==1]=255
    return img.astype('uint8')


# In[29]:



def removeBackground(orig_image , ratio = 0.5):
    
    #Resizing the image for faster clustering
    image  = orig_image.copy()  
    dim = (int(image.shape[1]*ratio),int(image.shape[0]*ratio))
    image = cv2.resize(image,dim ,interpolation = cv2.INTER_AREA)
     
    (h, w) = image.shape[:2]
    mask = np.zeros((h,w))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

    # reshape the image into a feature vector so that k-means
    # can be applied
    image = image.reshape((image.shape[0] * image.shape[1], 3))

    # apply k-means using the specified number of clusters and
    # then create the quantized image based on the predictions
    clt = MiniBatchKMeans(n_clusters = 2)
    labels = clt.fit_predict(image)
    labels = labels.reshape((h,w))
    
    #Finding the most common label which would be the background and making a mask out of it
    most_common_cluster_label = Counter(chain.from_iterable(labels)).most_common(1)[0][0]
    bg_label = most_common_cluster_label 
    mask = invertMask(labels,bg_label).astype('uint8')
    
    #Resizing it back to the original image dimensions since we had shrunk it above
    final_mask = cv2.resize(mask, (orig_image.shape[1] , orig_image.shape[0]))
    fin = cv2.bitwise_and(original_image,original_image,mask=final_mask.astype('int8'))
    return fin


# In[30]:



def getKMeansMask(orig_image , ratio = 0.5):
    
    #Resizing the image for faster clustering
    image  = orig_image.copy()  
    dim = (int(image.shape[1]*ratio),int(image.shape[0]*ratio))
    image = cv2.resize(image,dim ,interpolation = cv2.INTER_AREA)
     
    (h, w) = image.shape[:2]
    mask = np.zeros((h,w))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

    # reshape the image into a feature vector so that k-means
    # can be applied
    image = image.reshape((image.shape[0] * image.shape[1], 3))

    # apply k-means using the specified number of clusters and
    # then create the quantized image based on the predictions
    clt = MiniBatchKMeans(n_clusters = 2)
    labels = clt.fit_predict(image)
    labels = labels.reshape((h,w))
    
    #Finding the most common label which would be the background and making a mask out of it
    most_common_cluster_label = Counter(chain.from_iterable(labels)).most_common(1)[0][0]
    bg_label = most_common_cluster_label 
    mask = invertMask(labels,bg_label).astype('uint8')
    
    #Resizing it back to the original image dimensions since we had shrunk it above
    final_mask = cv2.resize(mask, (orig_image.shape[1] , orig_image.shape[0])).astype('int8')
    return final_mask



def findPotentialHolesFromObjectContour(canny_thresh):
    all_closed_contours = None
    actual_closed_contours = None
    largest_contour_index = None
    childs_of_largest_contour = None
    closed_childs_of_largest_contour = None
    
    contours_tree, hierarchy_tree = cv2.findContours(canny_thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    hierarchy_tree = hierarchy_tree[0]
    hierarchy_tree = np.hstack((hierarchy_tree,np.arange(len(hierarchy_tree)).reshape((len(hierarchy_tree),1))))
    
    new_contours_approx_tree = []
    for c in contours_tree:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.001 * peri, True)
        new_contours_approx_tree.append(approx)
    
    contours_comp, hierarchy_comp = cv2.findContours(canny_thresh,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
    hierarchy_comp = hierarchy_comp[0]
    hierarchy_comp = np.hstack((hierarchy_comp,np.arange(len(hierarchy_comp)).reshape((len(hierarchy_comp),1))))
    
    new_contours_approx_comp = []
    for c in contours_comp:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.001 * peri, True)
        new_contours_approx_comp.append(approx)
    
    (new_contours_approx_tree1,contour_sorted1,hierarchy_tree_sorted) = zip(*sorted(zip(new_contours_approx_tree,contours_tree,hierarchy_tree), 
                        key=lambda c : cv2.contourArea(c[0]), reverse = True))
    (new_contours_approx_comp1,contour_sorted2,hierarchy_comp_sorted) = zip(*sorted(zip(new_contours_approx_comp,contours_comp,hierarchy_comp), 
                        key=lambda c : cv2.contourArea(c[0]), reverse = True))


    all_closed_contours = [i for i,h in enumerate(hierarchy_comp_sorted) if h[2]>-1]
    actual_closed_contours =[hierarchy_tree_sorted[i][4] for i in all_closed_contours]
    final_list_closed_contours = []   

    for i in actual_closed_contours:
        final_list_closed_contours.append(getInnerContourIndex(i,hierarchy_tree_sorted))

    largest_contour_index = getInnerContourIndex(hierarchy_tree_sorted[0][4],hierarchy_tree_sorted)
    childs_of_largest_contour = [getInnerContourIndex(h[4],hierarchy_tree_sorted) for h in hierarchy_tree_sorted if h[3]==largest_contour_index]

    closed_childs_of_largest_contour =  [value for value in final_list_closed_contours if value in childs_of_largest_contour]
    return [contours_tree[i] for i in closed_childs_of_largest_contour]


# In[32]:


def getActualHoles(thresh , k_mask):
    req_contours = []
    c_img = None
    hole_match_mask = None
    n_pixels = None
    matched_pixels = None
    match_perc = None
    for c in findPotentialHolesFromObjectContour(thresh):
        c_img = np.zeros(thresh.shape)
        cv2.drawContours(c_img, [c], -1, color=(1), thickness=-1)
        n_pixels = np.sum(c_img)
        k_mask_c = k_mask.copy()
        k_mask_c = invertMask(k_mask_c,True)
        hole_match_mask = cv2.bitwise_and(c_img,c_img,mask = k_mask_c.astype('uint8'))
        matched_pixels = np.sum(hole_match_mask)
        match_perc = matched_pixels/n_pixels
        if match_perc >= 0.92:
            req_contours.append(c)
            
    return req_contours


# In[33]:


def getObjectOnlyMask(thresh):
    obj_only_mask = None
    #kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
    #thresh = cv2.dilate(thresh, kernel)
    largest_contour = getTheLargestContour(thresh)   
    obj_only_mask = createMaskfromContours([largest_contour] , thresh.shape[:2])

    return obj_only_mask


# In[34]:


def createMaskfromContours(contours , shape):
    mask = None
    mask = np.zeros(shape)
    for c in contours:
        cv2.fillPoly(mask, [c], (1))
    return mask


def getInnerContourIndex(parentIndex,hierarchy_tree_sort):
    
    parent = None
    child = None
    #get the first largest child of the parent with index parentIndex
    for h in hierarchy_tree_sort:
        if h[4]==parentIndex:
            parent = h
        if h[3]==parentIndex:
            child = h
            break
            
    if parent is None or child is None:
        return parentIndex
    elif child is not None:
        return child[4]
    '''
    if parent[2]==child[4] and child[3]==parent[4]:
        #choose the inner
        if child[0]==-1:
            #print('inner')
            return child[4]
    #print('outer') 
    '''
    return parentIndex


# In[100]:


def checkIfObjectInROI(obj_only_mask,roi_mask):
    lc_obj = getTheLargestContour(convertMasktoImage(obj_only_mask))
    lc_roi = getTheLargestContour(convertMasktoImage(roi_mask))
    n_p = len(lc_obj)
    not_match = 0
    for point in lc_obj:
        if cv2.pointPolygonTest(lc_roi,(point[0][0],point[0][1]),False) <0:
            not_match+=1
    if not_match/n_p < 0.05:
        return True
    else:
        return False