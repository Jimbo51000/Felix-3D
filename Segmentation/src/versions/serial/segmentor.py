#!/usr/bin/env python
# coding: utf-8

# In[1]:



import numpy as np
import argparse
import cv2
import os,shutil
import time
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import Counter
from itertools import chain

import util_segment as us
import util_grabcut as ug

from multiprocessing import Pool
import logging
import traceback

import argparse

# In[2]:


def preprocessImage(image,kernel):
    thresh = None
    thresh = np.max( np.array([ us.processImage(image,0), us.processImage(image,1) ,
                       us.processImage(image,2) ]), axis=0 )
    
    thresh = cv2.dilate(thresh, kernel)
    return thresh


# In[3]:


def useContourBasedSegmentation(original_image , thresh , k_means_mask , obj_only_mask ,kernel):
    holes = us.getActualHoles(thresh , k_means_mask)
    holes_only_mask = us.createMaskfromContours(holes , thresh.shape[:2])
    holes_only_mask = us.invertMask(holes_only_mask , True)

    #5.
    final_image_mask = np.ones(original_image.shape[:2])
    final_image_mask = cv2.bitwise_and(final_image_mask,final_image_mask,mask = obj_only_mask.astype('uint8'))
    final_image_mask = cv2.bitwise_and(final_image_mask,final_image_mask,mask = holes_only_mask.astype('uint8'))
    final_image_mask = cv2.erode(final_image_mask, kernel)

    #6.
    image_with_object = cv2.bitwise_and(original_image,original_image,mask = final_image_mask.astype('uint8'))
    return image_with_object


# In[4]:


def useGrabCutBasedSegmentation(path,shape,original_image,k_means_mask,roi_mask,group,i ):
    diff_mask = None
    fg = None
    roi= None
    new_mask1 = None
    image_with_object = None 
    
    (he,wi) = shape
    req_files = ug.getThePairFromFiles(group,i)
    diff_mask = ug.find_ssim_mask(path ,req_files,1.0)
    diff_mask = diff_mask[100:he-100,100:wi-100]
    fg = cv2.bitwise_and(k_means_mask,k_means_mask,mask = diff_mask.astype('uint8'))

    roi = cv2.bitwise_and(k_means_mask,k_means_mask,mask = roi_mask.astype('uint8'))

    new_mask1 = ug.getMaskFromGrabCut(original_image.copy(),k_means_mask.shape,roi,fg)

    image_with_object = original_image*new_mask1[:,:,np.newaxis]
    return image_with_object


# In[5]:


def createROIMask(path,group):
    roi_mask = None
    roi_mask = ug.find_ssim_mask(path,group)
    (he,wi) = roi_mask.shape[:2]
    roi_mask = roi_mask[100:he-100,100:wi-100]
    roi_mask = us.getObjectOnlyMask(roi_mask)
    roi_mask = roi_mask.astype('uint8')
    return roi_mask


# # Canny and Grabcut Combined Technique

# This is based on whether the actual object is within the ROI . If not contour detection has failed in obtaining
# the object thus we resort to grabcut.



def worker(task):
    
    try:
        g , group = task
        i = 0
        n = len(group)
        if roi_folder:
            roi_mask = cv2.imread(os.path.join(roi_folder,roi_files[g]),0)
        else:
            roi_mask = createROIMask(path,group)
        #print('ROI :'+roi_files[g])

        while i < n:
            try:
                original_image = None
                filename = group[i]
                original_image = cv2.imread(os.path.join(path,filename))

                if original_image is not None:

                    image = None
                    image_with_object = None
                    thresh = None
                    holes_only_mask = None
                    holes = None
                    k_means_mask = None
                    obj_only_mask = None

                    #1.
                    (he,wi,ch) = original_image.shape
                    original_image = original_image[100:he-100,100:wi-100,:]
                    hsv = cv2.cvtColor(original_image, cv2.COLOR_HSV2BGR)     
                    image = hsv.copy()

                    #2.
                    k_means_mask = us.getKMeansMask(image,scale_ratio)

                    #3.
                    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
                    thresh = preprocessImage(image , kernel)

                    #4.
                    obj_only_mask = us.getObjectOnlyMask(thresh.copy())

                    #5.
                    inside = us.checkIfObjectInROI(obj_only_mask,roi_mask)
                    technique = None
                    if inside:
                        #print(filename+' canny based')
                        technique = 'canny'
                        image_with_object = useContourBasedSegmentation(original_image, thresh , k_means_mask , obj_only_mask , kernel)
                    else:
                        #print(filename+' grabcut based')
                        technique = 'grabcut'
                        k_means_mask[k_means_mask==1] = 255
                        k_means_mask =  k_means_mask.astype('uint8')
                        image_with_object = useGrabCutBasedSegmentation(path,(he,wi),original_image,k_means_mask,roi_mask,group,i)


                    us.writeImage(outfolder , filename , image_with_object)
                    logging.info('%s done , technique = %s',filename,technique)
                    i+=1
            except Exception as e1:
                logging.error('%s failed , inside = %s',filename , inside , exc_info=True)
                continue
    except Exception as e:
        #print("here")
        logging.error('Thread failure for group %s ',group , exc_info=True)
        
        
        
#main function

ap = argparse.ArgumentParser(prog = "segmentor",description='Performs segmentation of foreground object from image')
ap.add_argument("-p", "--path", required=True,help="path to input image folder")
ap.add_argument("-o", "--out", required=True,help="path to output image folder.New one will be created if not exists")
ap.add_argument("-r", "--roi", required=False,help="path to roi image folder")
ap.add_argument("-g", "--group_size", required=True,type = int ,help="Size of sequence of images of each camera position")
ap.add_argument("-n", "--num_processors", required=False,type = int, default = 4,help="No of processes to be executed over")
ap.add_argument("-s", "--scale_ratio", required=False, help="Scale ratio to which the image must be resized for faster processing")

args = vars(ap.parse_args())
path = args["path"]
outfolder = args["out"]
roi_folder = args["roi"]
group_size = args["group_size"]
num_processors = args["num_processors"]
scale_ratio = args["scale_ratio"]
if not scale_ratio:
    scale_ratio = 0.3
    
if os.path.isdir(outfolder):
    shutil.rmtree(outfolder, ignore_errors=False, onerror=None)
os.mkdir(outfolder)
all = np.array([])

files = sorted(os.listdir(path))
if not roi_folder:
    roi_files = None
else:
    roi_files = sorted(os.listdir(roi_folder))

#n = len(files)
files = np.array(files)
m = (int)(len(files)/group_size)
files_group = np.split(files , [i*group_size for i in range(1,m)])

logging.basicConfig(filename='app.log', filemode='a', level = 'DEBUG' , format='%(asctime)s - %(levelname)s - %(message)s')

print('Started')
logging.info('Started Processing , \npath = %s , \n outpath = %s \n group_size = %d , num_processors = %d , scale_ratio = %.2f ',path,outfolder,
             group_size,num_processors,scale_ratio)
logging.info('n_files = %d',len(files))
start_time = time.time()
#Create a pool of processors
p=Pool(processes = num_processors)
#get them to work in parallel
tasks = [(i,group) for i,group in enumerate(files_group)]
output = p.map(worker,tasks)

#print("Completed in  %s seconds " % (time.time() - start_time))
logging.info("Completed in  %s seconds \n" , (time.time() - start_time))
print('Done')

