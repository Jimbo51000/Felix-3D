
import numpy as np
import argparse
import cv2
import os,shutil
import time
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import Counter
from itertools import chain
import rawpy
import imageio
import util_segment as us
from skimage.measure import compare_ssim

def find_ssim_mask(path,files,ratio = 1):
    final_thresh = None
    #files = sorted(os.listdir(path))
    n = len(files)
    i=1
    while i<n:
        img1 = None
        img2 = None
        img1 = cv2.imread(os.path.join(path,files[i]),0)
        img2 = cv2.imread(os.path.join(path,files[i-1]),0)
        (h,w) = img2.shape
        #dim = (int(img1.shape[1]*ratio),int(img1.shape[0]*ratio))
        #img1 = cv2.resize(img1,dim ,interpolation = cv2.INTER_AREA)
        #dim = (int(img2.shape[1]*ratio),int(img2.shape[0]*ratio))
        #img2 = cv2.resize(img2,dim ,interpolation = cv2.INTER_AREA)
        
        #print('    SSIM Pairing ',(files[i],files[i-1]))
        (score, diff1) = compare_ssim(img1, img2, full=True)
        diff1 = (diff1 * 255).astype("uint8")
        thresh1 = cv2.threshold(diff1, 0, 255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
        if i==1:
            final_thresh = thresh1
        else:
            final_thresh = sumUpDiff(final_thresh,thresh1)
        #displayImage(thresh1)
        #break
        i+=1

        #print(i)
    #print('Completed ssim')
    #final_thresh[final_thresh>0] = 255
    #final_thresh = cv2.resize(final_thresh, (w , h)).astype('int8')
    return final_thresh


def sumUpDiff(thresh1, thresh2):
    final_thresh1 = None
    final_thresh1 = cv2.add(thresh1,thresh2.astype('uint8'))
    final_thresh1[final_thresh1>0] = 255
    return final_thresh1

def getThePairFromFiles(files,i):
    req = []
    req.append(files[i])
    l = len(files)-1
    if i==l:
        req.append(files[i-1])
    else:
        req.append(files[i+1])
    return req

def getMaskFromGrabCut(original_image,shape,roi,fg):
    
    image_with_object = None
    image_with_object = np.zeros(shape)
    image_with_object[roi==0] = cv2.GC_BGD
    image_with_object[roi==255] =  cv2.GC_PR_BGD
    image_with_object[fg==255] = cv2.GC_FGD
    image_with_object = image_with_object.astype('uint8')

    #4.
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)

    new_mask, bgdModel, fgdModel = cv2.grabCut(original_image,image_with_object,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)

    new_mask = np.where((new_mask==2)|(new_mask==0),0,1).astype('uint8')
    
    return new_mask.copy()