#!/usr/bin/env python
# coding: utf-8

# In[1]:



import numpy as np
import argparse
import cv2
import os,shutil
import time
from sklearn.cluster import MiniBatchKMeans
import numpy as np
from collections import Counter
from itertools import chain

import util_segment as us
import util_grabcut as ug

import multiprocessing
from multiprocessing import Pool
import logging
import traceback

import argparse
from functools import reduce
#import progressbar

# In[2]:


def preprocessImage(image,kernel):
    thresh = None
    thresh = np.max( np.array([ us.processImage(image,0), us.processImage(image,1) ,
                       us.processImage(image,2) ]), axis=0 )
    
    thresh = cv2.dilate(thresh, kernel)
    return thresh


# In[3]:


def useContourBasedSegmentation(original_image , thresh , k_means_mask , obj_only_mask ,kernel):
    holes = us.getActualHoles(thresh , k_means_mask)
    holes_only_mask = us.createMaskfromContours(holes , thresh.shape[:2])
    holes_only_mask = us.invertMask(holes_only_mask , True)

    #5.
    final_image_mask = np.ones(original_image.shape[:2])
    final_image_mask = cv2.bitwise_and(final_image_mask,final_image_mask,mask = obj_only_mask.astype('uint8'))
    final_image_mask = cv2.bitwise_and(final_image_mask,final_image_mask,mask = holes_only_mask.astype('uint8'))
    final_image_mask = cv2.erode(final_image_mask, kernel)

    #6.
    image_with_object = cv2.bitwise_and(original_image,original_image,mask = final_image_mask.astype('uint8'))
    return image_with_object


# In[4]:
def findIndex(group_id,file_name):
    try:
        group = groups.get(group_id)
        idxes= np.where(group==file_name)
        return idxes[0][0]
    except Exception as e:
        logging.error('Filename %s not in group id : %d' , file_name , (group_id+1))
        return


def useGrabCutBasedSegmentation(file_name,path,shape,original_image,k_means_mask,roi_mask,group_id ):
    diff_mask = None
    fg = None
    roi= None
    new_mask1 = None
    image_with_object = None 
    
    (he,wi) = shape
    group = groups.get(group_id)
    i = findIndex(group_id,file_name)
    req_files = ug.getThePairFromFiles(group,i)
    diff_mask = ug.find_ssim_mask(path ,req_files,1.0)
    diff_mask = diff_mask[100:he-100,100:wi-100]
    fg = cv2.bitwise_and(k_means_mask,k_means_mask,mask = diff_mask.astype('uint8'))

    roi = cv2.bitwise_and(k_means_mask,k_means_mask,mask = roi_mask.astype('uint8'))

    new_mask1 = ug.getMaskFromGrabCut(original_image.copy(),k_means_mask.shape,roi,fg)

    image_with_object = original_image*new_mask1[:,:,np.newaxis]
    return image_with_object


# In[5]:


def createROIMask(path,group):
    roi_mask = None
    roi_mask = ug.find_ssim_mask(path,group)
    (he,wi) = roi_mask.shape[:2]
    roi_mask = roi_mask[100:he-100,100:wi-100]
    roi_mask = us.getObjectOnlyMask(roi_mask)
    roi_mask = roi_mask.astype('uint8')
    return roi_mask


# # Canny and Grabcut Combined Technique

# This is based on whether the actual object is within the ROI . If not contour detection has failed in obtaining
# the object thus we resort to grabcut.


# In[6]:
def worker_segment(task):

    g , file_name = task
    #i = 0
    #n = len(group)

    #print('ROI :'+roi_files[g])
    roi_mask = roi_masks.get(g)
    
    try:
        original_image = None
        #filename = group[i]
        original_image = cv2.imread(os.path.join(path,file_name))

        if original_image is not None:

            image = None
            image_with_object = None
            thresh = None
            holes_only_mask = None
            holes = None
            k_means_mask = None
            obj_only_mask = None

            #1.
            (he,wi,ch) = original_image.shape
            original_image = original_image[100:he-100,100:wi-100,:]
            hsv = cv2.cvtColor(original_image, cv2.COLOR_HSV2BGR)     
            image = hsv.copy()

            #2.
            k_means_mask = us.getKMeansMask(image,scale_ratio)

            #3.
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
            thresh = preprocessImage(image , kernel)

            #4.
            obj_only_mask = us.getObjectOnlyMask(thresh.copy())

            #5.
            inside = us.checkIfObjectInROI(obj_only_mask,roi_mask)
            technique = None
            if inside:
                #print(filename+' canny based')
                technique = 'canny'
                image_with_object = useContourBasedSegmentation(original_image, thresh , k_means_mask , obj_only_mask , kernel)
            else:
                #print(filename+' grabcut based')
                technique = 'grabcut'
                k_means_mask[k_means_mask==1] = 255
                k_means_mask =  k_means_mask.astype('uint8')
                image_with_object = useGrabCutBasedSegmentation(file_name,path,(he,wi),original_image,k_means_mask,roi_mask,g)


            us.writeImage(outfolder , file_name , image_with_object)
            logging.info('%s done ,roi_mask_id = %d, technique = %s',file_name,(g+1),technique)
            
    except Exception as e1:
        logging.error('%s failed ',file_name , exc_info=True)

# In[7]:
def worker_roi(file_group):
    try:
        return ug.find_ssim_mask(path,file_group)
    except Exception as e:
        logging.error('Thread failure for worker ROI sub-group %s ',file_group , exc_info=True)

# In[8]:
def run_roi_calculation():
    print('Calculating ROIs')
    logging.info('Calculating ROIs')
    for key in groups:
        try:
            #build subtasks
            g , group = key , groups[key]
            roi_mask = None
            if roi_folder:
                roi_mask = cv2.imread(os.path.join(roi_folder,roi_files[g]),0)
            else:
                group_size = len(group)
                sub_size = (int) (group_size/num_processors)
                sub_groups = np.split(group , [i*sub_size for i in range(1,num_processors)])
                with multiprocessing.Pool(processes = num_processors) as p:
                #get them to work in parallel
                    output_ssims = p.map(worker_roi,sub_groups)
                    roi_mask = reduce(ug.sumUpDiff, output_ssims)
                    (he, wi) = roi_mask.shape[:2]
                    roi_mask = roi_mask[100:he - 100, 100:wi - 100]
                    p.close()
                    p.join()


            roi_mask = us.getObjectOnlyMask(roi_mask)
            roi_mask = roi_mask.astype('uint8')
            roi_masks[g] = roi_mask
            #roi_bar.update(roi_curr_i+1)
            #roi_curr_i+=1
            logging.info('ROI Mask Calculation done for group %s' , group)
        except Exception as e:
            logging.error('Thread failure for run ROI group %s ',group , exc_info=True)
    logging.info('All ROI Mask Calculation done')
    print('ROI Calculation done')


def add_segmentation_tasks(task_queue):

    lst = list(map(lambda x : [(x[0],i) for i in x[1]],groups.items()))
    tasks = reduce(lambda a , b : a+b , lst)
    for task in tasks:
        #This is a tuple with group id , file name
        task_queue.put(task)

    return task_queue

def create_process_logger():
    logger = multiprocessing.get_logger()
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler('./logs/process.log' , mode = file_mode)
    fmt = '%(asctime)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def process_segmentation(task_queue):
    process_logger = create_process_logger()
    proc = os.getpid()
    while not task_queue.empty():
        try:
            task = task_queue.get()
            g , file_name = task
            worker_segment(task)
            process_logger.info('Done .Image = %s , roi_mask_id = %d , process_id = %d',file_name,(g+1),proc)
            #img_curr_i+=1
        except Exception as e:
            process_logger.error('Failed .Image = %s , roi_mask_id = %d , process_id = %d',file_name,(g+1),proc,exc_info=True)
            continue

    process_logger.info('Process %d completed successfully' , proc)
    return True


# In[9]:        
def run_segmentation():
    #Create a queue
    empty_task_queue = multiprocessing.Queue()
    full_task_queue = add_segmentation_tasks(empty_task_queue)
    processes = []
    for w in range(num_processors):
        p = multiprocessing.Process(
            target=process_segmentation, args=(full_task_queue,))
        processes.append(p)
        p.start()
        #image_bar.update(img_curr_i)
    for p in processes:
        p.join()


def writeROIMasks():
    i = 1
    for tup in roi_masks:
        g , roi = tup
        us.writeImage(outfolder , str(i)+'_roi_mask.png' , us.convertMasktoImage(roi))
    i+=1


# In[10]:
#main function
if __name__ == '__main__':
    ap = argparse.ArgumentParser(prog = "segmentor",description='Performs segmentation of foreground object from image')
    ap.add_argument("-p", "--path", required=True,help="path to input image folder")
    ap.add_argument("-o", "--out", required=True,help="path to output image folder.New one will be created if not exists")
    ap.add_argument("-r", "--roi", required=False,help="path to roi image folder")
    ap.add_argument("-g", "--group_size", required=True,type = int ,help="Size of sequence of images of each camera position")
    ap.add_argument("-n", "--num_processors", required=False,type = int, default = 4,help="No of processes to be executed over")
    ap.add_argument("-s", "--scale_ratio", required=False, help="Scale ratio to which the image must be resized for faster processing")
    ap.add_argument("-c", "--clearlogs", required=False, help="clear all the logs",action = 'store_true')

    args = vars(ap.parse_args())
    path = args["path"]
    outfolder = args["out"]
    roi_folder = args["roi"]
    group_size = args["group_size"]
    num_processors = args["num_processors"]
    scale_ratio = args["scale_ratio"]
    if not scale_ratio:
        scale_ratio = 0.3

    if args["clearlogs"]:
        file_mode = 'w'
    else:
        file_mode = 'a'
    #Global Variables
    
    roi_masks = {}
    groups = {}
    img_curr_i = 0
    roi_curr_i = 0

    PROCESSES = multiprocessing.cpu_count() - 1    
    num_processors = num_processors if num_processors < PROCESSES else PROCESSES    

    if os.path.isdir(outfolder):
        shutil.rmtree(outfolder, ignore_errors=False, onerror=None)
    os.mkdir(outfolder)

    logsfolder = './logs'
    if not os.path.isdir(logsfolder):
        os.mkdir(logsfolder)
    
    all = np.array([])

    files = sorted(os.listdir(path))
    if not roi_folder:
        roi_files = None
    else:
        roi_files = sorted(os.listdir(roi_folder))

    #All the images to be processed will be in groups varible and available globally
    files = np.array(files)
    m = (int)(len(files)/group_size)
    files_group = np.split(files , [i*group_size for i in range(1,m)])
    groups = {i:group for i,group in enumerate(files_group)}

    n_files = len(files)

    logging.basicConfig(filename='./logs/app.log', filemode=file_mode, level = 'DEBUG' , format='%(asctime)s - %(levelname)s - %(message)s')

    print('Started Processing ')
    logging.info('Started Processing , \npath = %s , \n outpath = %s \n group_size = %d , num_processors = %d , scale_ratio = %.2f ',path,outfolder,group_size,num_processors,scale_ratio)
    logging.info('n_files = %d',len(files))
    start_time = time.time()

    '''
    roi_bar = progressbar.ProgressBar(maxval=m, \
        widgets=[progressbar.Bar('=', 'ROI Masks :[', ']'), str(m)+'/', progressbar.Counter()])

    image_bar = progressbar.ProgressBar(maxval=n_files, \
        widgets=[progressbar.Bar('=', 'Images Segmented :[', ']'), str(n_files)+'/', progressbar.Counter()])
    '''
    #roi_bar.start()
    run_roi_calculation()
    #roi_bar.finish()

    #image_bar.start()
    run_segmentation()
    #image_bar.finish()

    #print("Completed in  %s seconds " % (time.time() - start_time))
    logging.info("Completed in  %s seconds \n" , (time.time() - start_time))
    print("Completed in  %s seconds " % (time.time() - start_time))

