from setuptools import setup

setup(name='Segmentor',
      version='0.1',
      description='Image Segmentation',
      url='https://gitlab.com/Jimbo51000/Felix-3D.git',
      author='Jimmy Joseph',
      author_email='jimmyj005@gmail.com',
      license='GPL',
      packages=['segmentor'],
      install_requires=[
          'numpy','argparse','os','shutil','time','scikit-learn','collections',
          'multiprocessing','logging','traceback','functools','rawpy','imageio',
          'scikit-image','opencv-python','more-itertools'
      ],
      zip_safe=False)